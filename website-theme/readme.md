 
------------------------------------------------------------------------------------

C Medlock - cm.medlock@ml1.net

Jan 2018

v 1.0
------------------------------------------------------------------------------------
MIT License

Copyright (c) 2018 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
------------------------------------------------------------------------------------



DO a BACKUP first!

Just copy the relevant folders to the respective areas of your site

Then do the modifications below:-

1) /includes/

	a) Adjust nav.html - menu contents
	b) Adjust intro.html,  contents - clock update 


2) /defaults/

	a) Adjust default.html - email address, clock update


3) /jscripts/

	a) jFunctions.js - adjust to your tastes!

		"DateTimeString" construct - add and remove what you want.






NB - Java script not completed, intend to add further functionality over time.