//
// Java script functions
//
// Chris Medlock
//
// April 2018
//
// Version 1.0
//
// ToDo
// ============================
// Days since new year
// Week number
// Sunrise & set
// Moonrise & set
// Julian date
// Sidereal time
// ============================
//
//
// Function to display date and time.
//
//
function updateClock()
{ 
  var DateTime = new Date();

  var unixTime = Math.round(+new Date()/1000);

  var Day = DateTime.get;

  var yy = DateTime.getUTCFullYear();
  var mm = DateTime.getUTCMonth() + 1;
  var dd = DateTime.getUTCDate();

  var hhs = DateTime.getHours();
  var mms = DateTime.getMinutes();

  var uhs = DateTime.getUTCHours();

  // Format elements < 2 bytes (ie < 10) -> insert preceding zero

  dd = ( dd <10 ? "0" : "" ) + dd
  mm = ( mm <10 ? "0" : "" ) + mm

  hhs = ( hhs < 10 ? "0" : "" ) + hhs;
  uhs = ( uhs < 10 ? "0" : "" ) + uhs;
  mms = ( mms < 10 ? "0" : "" ) + mms;



  // Construct strings to display

  var ddmmyy = dd + "/" + mm + "/" + yy;
  var localTime =  hhs + ":" + mms;
  var TimeGMTString = uhs + ":" + mms;
  var TimeUNIXString  = unixTime;

  var DateTimeString =  dd + "/" + mm + "/" + yy + " @ " + hhs + ":" + mms + ", GMT - " + TimeGMTString + ", UNIX - " + TimeUNIXString;


  // Update 
  document.getElementById("clock").firstChild.nodeValue = DateTimeString;





}

// Function to calculate days and weeks since new year.
//
//
function daysWeeks()
{


}
/////////////////////////////////////////////////////////////////////////////
// Below this line are third party functions
// 
// Credit assigned where applicable.



// Function to sort a table
//
// Courtesy of w3school
//

function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("tabless");  // tabless - my table id.
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
